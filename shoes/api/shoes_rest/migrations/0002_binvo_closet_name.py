# Generated by Django 4.0.3 on 2023-03-03 23:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='closet_name',
            field=models.CharField(default='Alpha', max_length=200),
        ),
    ]
