from django.db import models



class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name=models.CharField(max_length=200)
    bin_number=models.PositiveIntegerField()

class Shoe(models.Model):
    manufacture = models.CharField(max_length=200)
    model_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(max_length=200)
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )
