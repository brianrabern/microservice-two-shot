import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import Nav from './Nav';
import HatsList from './HatsList';
import CreateHatsForm from './CreateHatsForm';
import CreateShoesForm from './CreateShoesForm';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList />} />
          <Route path="shoes/new" element={<CreateShoesForm />} />
          <Route path="hats" element={<HatsList />} />
          <Route path="hats/new" element={<CreateHatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
