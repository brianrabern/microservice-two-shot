import { useEffect, useState } from 'react';

function HatsList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/hats_rest/hats/');

    if (response.ok) {
      const data = await response.json();
      setHats(data.hats)
    }
  }


  useEffect(()=>{
    getData()
  }, [])


 const handleDelete = async (e)=> {


    const url = `http://localhost:8090/hats_rest/hats/${e.target.id}`;

    const fetchConfigs = {
        method:"Delete",
        header: {
            "Content-Type": "application/json"
        }
    }

    const response = await fetch(url, fetchConfigs)
    const data = await response.json();

    setHats(hats.filter(hat => String(hat.pk) !== e.target.id))

 }



    const[locations, setLocations] = useState([])
    const getLocationData = async () => {
        const response = await fetch("http://localhost:8100/api/locations/");

        if (response.ok){
            const locationData = await response.json();
            setLocations(locationData.locations)
        }
    }

    useEffect(()=>{
        getLocationData()
      }, [])


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Location</th>
          <th>Section</th>
          <th>Shelf</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.pk}>
              <td><a href={hat.picture_url}>{ hat.style_name }</a></td>
              <td>{ hat.fabric }</td>
              <td>{ hat.color }</td>
              <td>{hat.location.closet_name}</td>
              <td>{hat.location.section_number}</td>
              <td>{hat.location.shelf_number}</td>
              <td><button onClick={handleDelete} id={hat.pk} className="btn btn-danger">Delete Item</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatsList;
