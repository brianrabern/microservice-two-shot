import { useEffect, useState } from 'react';


function ShoesList() {
  const [shoes, setShoes] = useState([])


  const getData = async () => {
    const response = await fetch('http://localhost:8080/shoes_rest/shoes/');
    if (response.ok)  {
      const data = await response.json();
      setShoes(data.shoes)

    };
  };


useEffect(()=>{
  getData()
}, [])


  const handleDelete = async (e) => {
    const url = `http://localhost:8080/shoes_rest/shoes/${e.target.id}/`

    const fetchConfigs = {
        method: "Delete",
        headers: {
            "Content-Type": "application/json"
        }
    }

    const response = await fetch(url, fetchConfigs)
    // const data = await response.json()

    setShoes(shoes.filter(shoe => String(shoe.pk) !== e.target.id))
    };



  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model Name </th>
          <th>Color</th>
          <th>Closet</th>
          <th>Bin</th>

          <th></th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => {
          return (
            <tr key={shoe.pk} >
              <td><a href={shoe.picture}>{ shoe.manufacture }</a></td>
              <td> { shoe.model_name }</td>
              <td>{ shoe.color }</td>
              <td> {shoe.bin.closet_name} </td>
              <td> {shoe.bin.bin_number} </td>
              <td><button onClick={handleDelete} id={shoe.pk} className="btn btn-danger">Delete</button></td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );

  };

export default ShoesList;
