import React, { useEffect, useState } from 'react';


function CreateShoesForm() {
    const [bins, setBins] = useState([]);

    const [formData, setFormData] = useState({
        manufacture: '',
        model_name: '',
        color: '',
        picture: '',
        bin: '',
    })

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/shoes_rest/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacture: '',
                model_name: '',
                color: '',
                picture: '',
                bin: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add shoes</h1>
            <form onSubmit={handleSubmit} id="create-shoes-form">

              <div className="form-floating mb-3">
                <input value={formData.manufacture} onChange={handleFormChange} placeholder="Fabric" required type="text" name="manufacture" id="manufacture" className="form-control"/>
                <label htmlFor="manufacture">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.model_name} onChange={handleFormChange} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Model name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.color} onChange={handleFormChange} placeholder="Color" type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.picture} onChange={handleFormChange} placeholder="Picture URL"  type="text" name="picture" id="picture" className="form-control"/>
                <label htmlFor="picture">Picture URL</label>
              </div>
              <div className="mb-3">
                <select value={formData.bin} onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a location </option>
                  {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>
                     Closet: {bin.closet_name} -- Bin: {bin.bin_number}
                    </option>
                  );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )



}
export default CreateShoesForm;


// {bin.closet_name}: {bin.bin_number}
