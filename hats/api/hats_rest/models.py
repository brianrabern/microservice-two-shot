from django.db import models



# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200, default="Aaron's closet")
    section_number = models.IntegerField(default=False)
    shelf_number = models.IntegerField(default=False)

class Hat(models.Model):
    # The hat model describes a specific hat
    fabric = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )
