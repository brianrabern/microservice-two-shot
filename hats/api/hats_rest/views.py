from django.shortcuts import render
from .models import Hat
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from .models import LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric", "style_name", "color",  "picture_url", "pk", "location"]


    encoders = {
        "location": LocationVODetailEncoder()
    }



class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "pk",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]


    encoders = {
        "location": LocationVODetailEncoder()
    }



# Create your views here.
@require_http_methods(["GET", "POST"])
def list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats" : hats},
            encoder = HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content['location'])
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400.
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def show_hats(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
